# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3,2,1,], [], []]
  end

  # def stop?(input)
  #   stop = %w(quit end stop stahp exit)
  #   stop.include?(input.to_s)
  # end

  def play
    count = 0
    until won?

      render

      puts "What pile shall we select your next disc from?"
      from_tower = gets.chomp.to_i
      # break if stop?(from_tower)

      puts "Okay, you picked tower #{from_tower}"
      puts "Which pile would you like to place this disc on?"
      to_tower = gets.chomp.to_i
      # break if stop?(to_tower)

      puts "Awesomesauce, you plan to put the tower on #{to_tower}"

      puts "Dude, you can't do that. Try again." if valid_move?(from_tower, to_tower) == false
      next if valid_move?(from_tower, to_tower) == false

      move(from_tower, to_tower)

      count += 1

      if won?
        puts "Rad dude, you hecka won!"
        puts "You moved the tower in #{count} moves"
      end

    end

    return "You get a gold star" #made it through the until loop

  end

  def move(from_tower, to_tower)
    disc = @towers[from_tower].pop
    @towers[to_tower].push(disc)
  end

  def valid_move?(from_tower, to_tower)
    if towers[from_tower] == []
      puts "That's an empty tower. Pick a new one."
      return false
    end
    if out_of_bounds(from_tower) || out_of_bounds(to_tower)
      puts "That's not a valid tower number, try using 0, 1 or 2"
      return false
    end
    towers[to_tower].last.nil? \
    || towers[from_tower].last < towers[to_tower].last
  end

  def out_of_bounds(tower)
    tower > 2 || tower < 0
  end


  def won?
    towers == [[], [], [3,2,1,]] || towers == [[], [3,2,1,], []]
  end

  def render
    puts "This is what the towers look like now:"
    puts "Tower 0 : #{towers[0]}"
    puts "Tower 1 : #{towers[1]}"
    puts "Tower 2 : #{towers[2]}"
  end

end

if __FILE__ == $PROGRAM_NAME
  TowersOfHanoi.new.play
end
